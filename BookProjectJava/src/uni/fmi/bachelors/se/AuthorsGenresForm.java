package uni.fmi.bachelors.se;

import java.awt.Component;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AuthorsGenresForm extends JFrame
{

	private JPanel contentPane;
	private JTable table;
	BooksForm parentForm;
	ArrayList<Author> authors;
	ArrayList<Genre> genres;
	int marker;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					AuthorsGenresForm frame = new AuthorsGenresForm(new BooksForm(), 0);
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}	

	public AuthorsGenresForm(BooksForm form, int marker)
	{
		parentForm = form;
		this.marker = marker;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 622, 374);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable();

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(23, 11, 419, 292);
		contentPane.add(scrollPane);

		JButton btnAddNew = new JButton("Add New");
		btnAddNew.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				AuthorsGenresForm.this.setVisible(false);
				if (marker == 1)
				{
					AuthorGenreAddEditForm addEditForm = new AuthorGenreAddEditForm(AuthorsGenresForm.this, 1);
					addEditForm.setVisible(true);
				} else if (marker == 2)
				{
					AuthorGenreAddEditForm addEditForm = new AuthorGenreAddEditForm(AuthorsGenresForm.this, 2);
					addEditForm.setVisible(true);
				}
			}
		});
		btnAddNew.setBounds(479, 75, 89, 23);
		contentPane.add(btnAddNew);

		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (table.getSelectedRow() == -1)
				{
					NotSelectedError();
				}else {
					int row = table.getSelectedRow();
					long id = (long) table.getValueAt(row, 0);
					if (marker == 1)
						editAuthor(id);
					else if (marker == 2)
						editGenre(id);
				}
			}
		});
		btnEdit.setBounds(479, 140, 89, 23);
		contentPane.add(btnEdit);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (table.getSelectedRow() == -1)
				{
					NotSelectedError();
				}else {
					int row = table.getSelectedRow();
					long id = (long) table.getValueAt(row, 0);
					if (marker == 1)
					{
						deleteAuthor(id);
						fillAuthorData();
					} else if (marker == 2)
					{
						deleteGenre(id);
						fillGenreData();
					}
				}
			}
		});
		btnDelete.setBounds(479, 200, 89, 23);
		contentPane.add(btnDelete);

		JButton btnBackToBooks = new JButton("Back to Books");
		btnBackToBooks.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				AuthorsGenresForm.this.setVisible(false);
				parentForm.setVisible(true);
				parentForm.fillBookData();
			}
		});
		btnBackToBooks.setBounds(467, 262, 106, 23);
		contentPane.add(btnBackToBooks);

		if (marker == 1)
			fillAuthorData();
		else if (marker == 2)
			fillGenreData();
	}

	public void fillAuthorData()
	{
		authors = AuthorRepository.GetAll();
		DefaultTableModel model = new DefaultTableModel(new String[]
		{ "Id", "Name" }, 0);

		for (Author author : authors)
		{
			Object[] row = new Object[2];
			row[0] = author.id;
			row[1] = author.name;
			model.addRow(row);
		}
		table.setModel(model);
		table.getColumnModel().getColumn(0).setMaxWidth(30);

	}

	public void editAuthor(long id)
	{
		Author author = AuthorRepository.GetById(id);
		if (author != null)
		{
			AuthorGenreAddEditForm editAuthorform = new AuthorGenreAddEditForm(this, author);
			this.setVisible(false);
			editAuthorform.setVisible(true);
		}
	}

	public void deleteAuthor(long id)
	{
		// to make JDialog with confirmation
		AuthorRepository.Delete(id);
	}

	public void fillGenreData()
	{
		genres = GenreRepository.GetAll();
		DefaultTableModel model = new DefaultTableModel(new String[]
		{ "Id", "Name" }, 0);

		for (Genre genre : genres)
		{
			Object[] row = new Object[2];
			row[0] = genre.id;
			row[1] = genre.name;
			model.addRow(row);
		}
		table.setModel(model);
		table.getColumnModel().getColumn(0).setMaxWidth(30);

	}

	public void editGenre(long id)
	{
		Genre genre = GenreRepository.GetById(id);
		if (genre != null)
		{
			AuthorGenreAddEditForm editGenreform = new AuthorGenreAddEditForm(this, genre);
			this.setVisible(false);
			editGenreform.setVisible(true);
		}
	}

	public void deleteGenre(long id)
	{
		// to make JDialog with confirmation
		GenreRepository.Delete(id);
	}
	
	public void NotSelectedError() {
		JOptionPane optionPane = new JOptionPane("You have not selected an instance!", JOptionPane.ERROR_MESSAGE);    
		JDialog dialog = optionPane.createDialog("Error!");
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);
	}
}
