package uni.fmi.bachelors.se;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GenreRepository
{
	public static ArrayList<Genre> GetAll()
	{
		Connection conn = null;
		Genre genre = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select g.id, g.name from genre g";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Genre> genres = new ArrayList<Genre>();

			while (rs.next())
			{
				genre = new Genre();
				genre.id = rs.getLong(rs.findColumn("Id"));
				genre.name = rs.getString(rs.findColumn("Name"));
				genres.add(genre);
			}
			return genres;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Genre> GetGenresByBookId(long id)
	{
		Connection conn = null;
		Genre genre = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select g.id, g.name from book b " + "join bookgenre bg on b.id=bg.bookid "
					+ "join genre g on g.id = bg.genreid " + "where b.id =" + id;

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Genre> genres = new ArrayList<Genre>();

			while (rs.next())
			{
				genre = new Genre();
				genre.id = rs.getLong(rs.findColumn("Id"));
				genre.name = rs.getString(rs.findColumn("Name"));
				genres.add(genre);
			}
			return genres;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Genre GetById(long id)
	{
		Connection conn = null;
		Genre genre = null;

		try
		{
			conn = ConnectionString.getConnection();
			String query = "select g.id, g.name from genre g where g.id=" + id;
			PreparedStatement pst = conn.prepareStatement(query);

			ResultSet rs = pst.executeQuery();

			if (rs.first())
			{
				genre = new Genre();
				genre.id = rs.getLong(rs.findColumn("Id"));
				genre.name = rs.getString(rs.findColumn("Name"));
				return genre;
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void Insert(Genre genre)
	{
		try
		{
			Connection conn = ConnectionString.getConnection();

			String query = "insert into genre (name) values ('" + genre.name + "')";

			PreparedStatement pst = conn.prepareStatement(query);

			pst.execute();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void Update(Genre genre)
	{
		Connection conn = null;
		try
		{
			conn = ConnectionString.getConnection();

			String query = "update genre set name='" + genre.name + "' where id=" + genre.id;

			PreparedStatement pst = conn.prepareStatement(query);

			pst.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void Delete(long id)
	{
		Connection conn = null;
		try
		{
			conn = ConnectionString.getConnection();
			String query = "delete from bookgenre where genreid ="+id
					+ "; Delete from genre where id=" + id;

			PreparedStatement pst = conn.prepareStatement(query);

			pst.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

}
