package uni.fmi.bachelors.se;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AuthorRepository
{
	public static ArrayList<Author> GetAll()
	{
		Connection conn = null;
		Author author = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select a.id, a.name from author a";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Author> genres = new ArrayList<Author>();

			while (rs.next())
			{
				author = new Author();
				author.id = rs.getLong(rs.findColumn("Id"));
				author.name = rs.getString(rs.findColumn("Name"));
				genres.add(author);
			}
			return genres;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Author> GetAuthorsByBookId(long id)
	{
		Connection conn = null;
		Author author = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select a.id, a.name from book b " + "join bookauthor ba on b.id=ba.bookid "
					+ "join author a on a.id = ba.authorid " + "where b.id =" + id;

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Author> authors = new ArrayList<Author>();

			while (rs.next())
			{
				author = new Author();
				author.id = rs.getLong(rs.findColumn("Id"));
				author.name = rs.getString(rs.findColumn("Name"));
				authors.add(author);
			}
			return authors;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static Author GetById(long id)
	{
		Connection conn = null;
		Author author = null;

		try
		{
			conn = ConnectionString.getConnection();
			String query = "select a.id, a.name from author a where a.id=" + id;
			PreparedStatement pst = conn.prepareStatement(query);

			ResultSet rs = pst.executeQuery();

			if (rs.first())
			{
				author = new Author();
				author.id = rs.getLong(rs.findColumn("Id"));
				author.name = rs.getString(rs.findColumn("Name"));
				return author;
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void Insert(Author author)
	{
		try
		{
			Connection conn = ConnectionString.getConnection();

			String query = "insert into author (name) values ('" + author.name + "')";

			PreparedStatement pst = conn.prepareStatement(query);

			pst.execute();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void Update(Author author)
	{
		Connection conn = null;
		try
		{
			conn = ConnectionString.getConnection();

			String query = "update author set name='" + author.name + "' where id=" + author.id;

			PreparedStatement pst = conn.prepareStatement(query);

			pst.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void Delete(long id)
	{
		Connection conn = null;
		try
		{
			conn = ConnectionString.getConnection();
			String query = "delete from bookauthor where authorid="+id
					+ "; Delete from author where id=" + id;

			PreparedStatement pst = conn.prepareStatement(query);

			pst.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

}
