package uni.fmi.bachelors.se;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookRepository
{
	public static ArrayList<Book> GetAll()
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select b.id, b.title from book b";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Book GetById(long id)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();
			String query = "select b.id, b.title from book b where b.id=" + id;
			PreparedStatement pst = conn.prepareStatement(query);

			ResultSet rs = pst.executeQuery();

			if (rs.first())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				return book;
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Long GetIdByTitle(String title)
	{
		Connection conn = null;
		Long bookId;

		try
		{
			conn = ConnectionString.getConnection();
			String query = "select b.id from book b where b.title='" + title + "'";
			PreparedStatement pst = conn.prepareStatement(query);

			ResultSet rs = pst.executeQuery();

			if (rs.first())
			{
				bookId = rs.getLong(rs.findColumn("Id"));
				return bookId;
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByAuthorTitleAndGenre(String genre, String author, String title)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title "
					+ "from book b, bookauthor ba, author a, bookgenre bg, genre g " + "where b.id=ba.bookid "
					+ "and a.id = ba.authorid " + "and b.id = bg.bookid " + "and g.id = bg.genreid "
					+ "and a.name ilike '%" + author + "%' " + "and b.title ilike '%" + title + "%' "
					+ "and g.name ilike '%" + genre + "%'";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByAuthorAndGenre(String genre, String author)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title "
					+ "from book b, bookauthor ba, author a, bookgenre bg, genre g " + "where b.id=ba.BOOKID "
					+ "and a.id=ba.AUTHORID " + "and g.id=bg.GENREID " + "and b.id = bg.BOOKID " + "and a.name ilike '%"
					+ author + "%' " + "and g.name ilike '%" + genre + "%'";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByAuthorAndTitle(String title, String author)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title " + "from book b, bookauthor ba, author a "
					+ "where b.id=ba.BOOKID " + "and a.id=ba.AUTHORID " + "and a.name ilike '%" + author + "%' "
					+ "and b.title ilike '%" + title + "%'";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByTitleAndGenre(String genre, String title)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title from book b, bookgenre bg, "
					+ "genre g where b.id = bg.BOOKID and g.id=bg.GENREID "
					+ "and b.title ilike '%"+title+"%' and g.name ilike '%"+genre+"%'";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByTitle(String title)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title from book b " + "where b.title ilike '%" + title + "%'";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByAuthor(String author)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title " + "from book b, bookauthor ba, author a "
					+ "where b.id=ba.BOOKID " + "and a.id=ba.AUTHORID " + "and a.name ilike '%" + author + "%' ";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ArrayList<Book> GetBookByGenre(String genre)
	{
		Connection conn = null;
		Book book = null;

		try
		{
			conn = ConnectionString.getConnection();

			String query = "select distinct b.id, b.title " + 
					"from book b, bookgenre bg, genre g " +
					"where g.id=bg.GENREID " + 
					"and b.id = bg.BOOKID " +
					"and g.name ilike '%" + genre + "%'";

			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>();

			while (rs.next())
			{
				book = new Book();
				book.id = rs.getLong(rs.findColumn("Id"));
				book.title = rs.getString(rs.findColumn("Title"));
				books.add(book);
			}
			return books;

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void Insert(Book book)
	{
		try
		{
			Connection conn = ConnectionString.getConnection();

			String query = "insert into book (title) values ('" + book.title + "')";

			PreparedStatement pst = conn.prepareStatement(query);

			pst.execute();

			query = "insert";

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void InsertToBookAuthors(Book book, ArrayList<Long> authorIds)
	{
		Long bookId = GetIdByTitle(book.title);
		try
		{
			Connection conn = ConnectionString.getConnection();

			for (Long id : authorIds)
			{
				String query = "insert into bookauthor (bookid, authorid) values (" + bookId + "," + id + ")";
				PreparedStatement pst = conn.prepareStatement(query);
				pst.execute();
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void RemoveFromBookAuthors(Book book, ArrayList<Long> authorIds)
	{
		try
		{
			Connection conn = ConnectionString.getConnection();

			for (Long id : authorIds)
			{
				String query = "delete from bookauthor where bookid =" + book.id + " and authorid = " + id;
				PreparedStatement pst = conn.prepareStatement(query);
				pst.execute();
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void InsertToBookGenres(Book book, ArrayList<Long> genreIds)
	{
		Long bookId = GetIdByTitle(book.title);
		try
		{
			Connection conn = ConnectionString.getConnection();

			for (Long id : genreIds)
			{
				String query = "insert into bookgenre (bookid, genreid) values (" + bookId + ", " + id + ")";
				PreparedStatement pst = conn.prepareStatement(query);
				pst.execute();
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void RemoveFromBookGenres(Book book, ArrayList<Long> genreIds)
	{
		try
		{
			Connection conn = ConnectionString.getConnection();

			for (Long id : genreIds)
			{
				String query = "delete from bookgenre where bookid =" + book.id + " and genreid = " + id;
				PreparedStatement pst = conn.prepareStatement(query);
				pst.execute();
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void UpdateBookAuthors(Book book, ArrayList<Long> newAuthorIds)
	{
		ArrayList<Long> oldAuthorIds = new ArrayList<Long>();
		for (Author author : AuthorRepository.GetAuthorsByBookId(book.id))
		{
			oldAuthorIds.add(author.id);
		}
		ArrayList<Long> temp = new ArrayList<Long>();
		temp.addAll(oldAuthorIds);
		oldAuthorIds.removeAll(newAuthorIds);
		newAuthorIds.removeAll(temp);

		if (!oldAuthorIds.isEmpty())
			RemoveFromBookAuthors(book, oldAuthorIds);
		if (!newAuthorIds.isEmpty())
			InsertToBookAuthors(book, newAuthorIds);
	}

	public static void UpdateBookGenres(Book book, ArrayList<Long> newGenreIds)
	{
		ArrayList<Long> oldGenreIds = new ArrayList<Long>();
		for (Genre genre : GenreRepository.GetGenresByBookId(book.id))
		{
			oldGenreIds.add(genre.id);
		}
		ArrayList<Long> temp = new ArrayList<Long>();
		temp.addAll(oldGenreIds);
		oldGenreIds.removeAll(newGenreIds);
		newGenreIds.removeAll(temp);

		if (!oldGenreIds.isEmpty())
			RemoveFromBookGenres(book, oldGenreIds);
		if (!newGenreIds.isEmpty())
			InsertToBookGenres(book, newGenreIds);
	}

	public static void Update(Book book)
	{
		Connection conn = null;
		try
		{
			conn = ConnectionString.getConnection();

			String query = "update book set title='" + book.title + "' where id=" + book.id;

			PreparedStatement pst = conn.prepareStatement(query);

			pst.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void Delete(long id)
	{

		Connection conn = null;
		try
		{
			conn = ConnectionString.getConnection();
			String query = "delete from bookauthor where bookid =" + id + "; delete from bookgenre where bookid =" + id
					+ "; delete from book where id=" + id;

			PreparedStatement pst = conn.prepareStatement(query);

			pst.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
}
