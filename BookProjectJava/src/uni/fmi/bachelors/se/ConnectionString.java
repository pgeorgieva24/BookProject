package uni.fmi.bachelors.se;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionString {

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:h2:~/BooksDB", "sa", "");
	}
}
