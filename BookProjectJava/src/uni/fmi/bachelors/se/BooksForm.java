package uni.fmi.bachelors.se;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class BooksForm extends JFrame
{

	private JPanel contentPane;
	private JTable bookTable;
	private JTextField textFieldAuthor;
	private JTextField textFieldGenre;
	private JTextField textFieldTitle;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					BooksForm frame = new BooksForm();
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public BooksForm()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 901, 579);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		bookTable = new JTable();

		JScrollPane scrollPane = new JScrollPane(bookTable);
		scrollPane.setBounds(23, 11, 695, 427);
		contentPane.add(scrollPane);

		JButton btnAddBook = new JButton("Add Book");
		btnAddBook.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				BookAddEditForm addEditForm = new BookAddEditForm(BooksForm.this);
				addEditForm.setVisible(true);
				BooksForm.this.setVisible(false);
			}
		});
		btnAddBook.setBounds(743, 73, 89, 23);
		contentPane.add(btnAddBook);

		JButton btnEditBook = new JButton("Edit Book");
		btnEditBook.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (bookTable.getSelectedRow() == -1)
				{
					NotSelectedError();
				} else
				{
					int row = bookTable.getSelectedRow();
					long id = (long) bookTable.getValueAt(row, 0);
					Book book = BookRepository.GetById(id);
					book.authors = AuthorRepository.GetAuthorsByBookId(book.id);
					book.genres = GenreRepository.GetGenresByBookId(book.id);
					BookAddEditForm addEditForm = new BookAddEditForm(BooksForm.this, book);
					addEditForm.setVisible(true);
					BooksForm.this.setVisible(false);
				}
			}
		});
		btnEditBook.setBounds(743, 121, 89, 23);
		contentPane.add(btnEditBook);

		JButton btnAuthors = new JButton("Authors");
		btnAuthors.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				AuthorsGenresForm getAuthors = new AuthorsGenresForm(BooksForm.this, 1);
				getAuthors.setVisible(true);
				BooksForm.this.setVisible(false);
			}
		});
		btnAuthors.setBounds(743, 260, 89, 23);
		contentPane.add(btnAuthors);

		JButton btnGenres = new JButton("Genres");
		btnGenres.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				AuthorsGenresForm getGenres = new AuthorsGenresForm(BooksForm.this, 2);
				getGenres.setVisible(true);
				BooksForm.this.setVisible(false);
			}
		});
		btnGenres.setBounds(743, 315, 89, 23);
		contentPane.add(btnGenres);

		JButton btnDeleteBook = new JButton("Delete Book");
		btnDeleteBook.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (bookTable.getSelectedRow() == -1)
				{
					NotSelectedError();
				} else
				{
					int row = bookTable.getSelectedRow();
					long id = (long) bookTable.getValueAt(row, 0);
					BookRepository.Delete(id);
					fillBookData();
				}
			}
		});
		btnDeleteBook.setBounds(743, 178, 89, 23);
		contentPane.add(btnDeleteBook);
		
		textFieldAuthor = new JTextField();
		textFieldAuthor.setBounds(154, 449, 86, 20);
		contentPane.add(textFieldAuthor);
		textFieldAuthor.setColumns(10);
		
		textFieldGenre = new JTextField();
		textFieldGenre.setColumns(10);
		textFieldGenre.setBounds(154, 480, 86, 20);
		contentPane.add(textFieldGenre);
		
		textFieldTitle = new JTextField();
		textFieldTitle.setColumns(10);
		textFieldTitle.setBounds(154, 511, 86, 20);
		contentPane.add(textFieldTitle);
		
		JLabel lblEnterBookauthor = new JLabel("Enter Book Author");
		lblEnterBookauthor.setBounds(33, 449, 121, 14);
		contentPane.add(lblEnterBookauthor);
		
		JLabel lblEnterBookGenre = new JLabel("Enter Book Genre");
		lblEnterBookGenre.setBounds(33, 480, 121, 14);
		contentPane.add(lblEnterBookGenre);
		
		JLabel lblEnterBookTitle = new JLabel("Enter Book Title");
		lblEnterBookTitle.setBounds(33, 511, 121, 14);
		contentPane.add(lblEnterBookTitle);
		
		JButton btnFilter = new JButton("Filter");
		btnFilter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String author = textFieldAuthor.getText();
				String genre = textFieldGenre.getText();
				String title = textFieldTitle.getText();
				ArrayList<Book> books = new ArrayList<Book>();
				if(!author.isEmpty()&&!genre.isEmpty()&&!title.isEmpty()) {
					books = BookRepository.GetBookByAuthorTitleAndGenre(genre, author, title);
					fillFilteredBookData(books);
				}else if(!author.isEmpty()&&!genre.isEmpty()&&title.isEmpty()) {
					books = BookRepository.GetBookByAuthorAndGenre(genre, author);
					fillFilteredBookData(books);
				}else if(!author.isEmpty()&&genre.isEmpty()&&!title.isEmpty()) {
					books = BookRepository.GetBookByAuthorAndTitle(title, author);
					fillFilteredBookData(books);
				}else if(author.isEmpty()&&!genre.isEmpty()&&!title.isEmpty()) {
					books = BookRepository.GetBookByTitleAndGenre(genre, title);
					fillFilteredBookData(books);
				}else if(author.isEmpty()&&genre.isEmpty()&&!title.isEmpty()) {
					books = BookRepository.GetBookByTitle(title);
					fillFilteredBookData(books);
				}else if(author.isEmpty()&&!genre.isEmpty()&&title.isEmpty()) {
					books = BookRepository.GetBookByGenre(genre);
					fillFilteredBookData(books);
				}else if(!author.isEmpty()&&genre.isEmpty()&&title.isEmpty()) {
					books = BookRepository.GetBookByAuthor(author);
					fillFilteredBookData(books);
				}else if(author.isEmpty()&&genre.isEmpty()&&title.isEmpty()) {
					fillBookData();
				}
			}
		});
		btnFilter.setBounds(326, 479, 89, 23);
		contentPane.add(btnFilter);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldAuthor.setText(null);
				textFieldGenre.setText(null);
				textFieldTitle.setText(null);
				ArrayList<Book> books = BookRepository.GetAll();
				fillFilteredBookData(books);
			}
		});
		btnClear.setBounds(468, 479, 89, 23);
		contentPane.add(btnClear);
		fillBookData();
	}

	public void fillBookData()
	{
		ArrayList<Book> books = BookRepository.GetAll();
		DefaultTableModel model = new DefaultTableModel(new String[]
		{ "Id", "Title", "Author", "Genre" }, 0);

		for (Book book : books)
		{
			String authors = "", genres = "";
			for (Author author : AuthorRepository.GetAuthorsByBookId(book.id))
			{
				authors += author.name + " ";
			}
			for (Genre genre : GenreRepository.GetGenresByBookId(book.id))
			{
				genres += genre.name + " ";
			}
			Object[] row = new Object[4];
			row[0] = book.id;
			row[1] = book.title;
			row[2] = authors;
			row[3] = genres;
			model.addRow(row);
		}
		bookTable.setModel(model);
		bookTable.getColumnModel().getColumn(0).setMaxWidth(30);
	}
	
	public void fillFilteredBookData(ArrayList<Book> books)
	{
		fillBookData();
		DefaultTableModel model = new DefaultTableModel(new String[]
		{ "Id", "Title", "Author", "Genre" }, 0);

		for (Book book : books)
		{
			String authors = "", genres = "";
			for (Author author : AuthorRepository.GetAuthorsByBookId(book.id))
			{
				authors += author.name + " ";
			}
			for (Genre genre : GenreRepository.GetGenresByBookId(book.id))
			{
				genres += genre.name + " ";
			}
			Object[] row = new Object[4];
			row[0] = book.id;
			row[1] = book.title;
			row[2] = authors;
			row[3] = genres;
			model.addRow(row);
		}
		bookTable.setModel(model);
		bookTable.getColumnModel().getColumn(0).setMaxWidth(30);
	}

	public void NotSelectedError()
	{
		JOptionPane optionPane = new JOptionPane("You have not selected a Book instance!", JOptionPane.ERROR_MESSAGE);
		JDialog dialog = optionPane.createDialog("Error!");
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);
	}
}
