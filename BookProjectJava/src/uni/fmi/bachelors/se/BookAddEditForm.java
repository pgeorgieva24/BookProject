package uni.fmi.bachelors.se;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class BookAddEditForm extends JFrame {

	BooksForm parentForm;
	Book book;
	ArrayList<Author> authors = AuthorRepository.GetAll();
	ArrayList<Genre> genres = GenreRepository.GetAll();
	ArrayList<JCheckBox> authorCheckboxes = new ArrayList<JCheckBox>();
	ArrayList<JCheckBox> genreCheckboxes = new ArrayList<JCheckBox>();
	int moveWith = 0;

	private JPanel contentPane;
	private JTextField tfTitle;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookAddEditForm frame = new BookAddEditForm(new BooksForm());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @wbp.parser.constructor
	 */
	public BookAddEditForm(BooksForm form, Book book) {
		this(form);
		this.book = book;
		tfTitle.setText(book.title);

		for (JCheckBox cb : authorCheckboxes) {
			if (book.authors.stream().anyMatch(item -> item.name.equals(cb.getText()))) {
				cb.setSelected(true);
			}
		}

		for (JCheckBox cb : genreCheckboxes) {
			if (book.genres.stream().anyMatch(item -> item.name.equals(cb.getText()))) {
				cb.setSelected(true);
			}
		}
	}

	public BookAddEditForm(BooksForm form) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 486, 624);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(82, 34, 46, 14);
		contentPane.add(lblTitle);

		tfTitle = new JTextField();
		tfTitle.setBounds(176, 31, 86, 20);
		contentPane.add(tfTitle);
		tfTitle.setColumns(10);

		JLabel lblAuthors = new JLabel("Authors");
		lblAuthors.setBounds(68, 94, 46, 14);
		contentPane.add(lblAuthors);

		JLabel lblGenres = new JLabel("Genres");
		lblGenres.setBounds(275, 94, 46, 14);
		contentPane.add(lblGenres);

		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfTitle.getText().length() > 0/* i ima cheknat po edin avtor i janr */) {
					if (book == null)
						book = new Book();

					book.title = tfTitle.getText();
					saveBook(book);
					BookAddEditForm.this.setVisible(false);
					parentForm.setVisible(true);
					parentForm.fillBookData();
				} else
					System.out.println("Title field cannot be empty");
			}

		});
		btnSubmit.setBounds(68, 478, 89, 23);
		contentPane.add(btnSubmit);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BookAddEditForm.this.setVisible(false);
				parentForm.setVisible(true);
				parentForm.fillBookData();
			}
		});
		btnCancel.setBounds(306, 478, 89, 23);
		contentPane.add(btnCancel);

		for (Author author : authors)
		{
			JCheckBox checkbox = new JCheckBox(author.name);
			checkbox.setBounds(59, 166 + moveWith, 150, 23);
			contentPane.add(checkbox);
			authorCheckboxes.add(checkbox);
			moveWith += 30;
		}

		moveWith = 0;
		for (Genre genre : genres)
		{
			JCheckBox checkbox = new JCheckBox(genre.name);
			checkbox.setBounds(300, 166 + moveWith, 150, 23);
			contentPane.add(checkbox);
			genreCheckboxes.add(checkbox);
			moveWith += 30;
		}

		parentForm = form;
	}

	private void saveBook(Book book) {
		HashMap<String, Long> authorMap = new HashMap<String, Long>();
		ArrayList<Long> authorIds = new ArrayList<Long>();
		ArrayList<Long> genreIds = new ArrayList<Long>();

		for (Author author : AuthorRepository.GetAll())
		{
			authorMap.put(author.name, author.id);
		}
		HashMap<String, Long> genreMap = new HashMap<String, Long>();
		for (Genre genre : GenreRepository.GetAll())
		{
			genreMap.put(genre.name, genre.id);
		}

		for (JCheckBox checkbox : authorCheckboxes)
		{
			if (checkbox.isSelected())
			{
				authorIds.add(authorMap.get(checkbox.getText()));
			}
		}
		for (JCheckBox checkbox : genreCheckboxes)
		{
			if (checkbox.isSelected())
			{
				genreIds.add(genreMap.get(checkbox.getText()));
			}
		}
		if (book.id != -1)
		{
			BookRepository.Update(book);
			BookRepository.UpdateBookAuthors(book, authorIds);
			BookRepository.UpdateBookGenres(book, genreIds);
		} else
		{
			BookRepository.Insert(book);
			BookRepository.InsertToBookAuthors(book, authorIds);
			BookRepository.InsertToBookGenres(book, genreIds);
		}
	}
}
