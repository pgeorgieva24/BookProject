package uni.fmi.bachelors.se;

import java.util.ArrayList;

public class Author {
	long id = -1;
	String name;
	ArrayList<Book> authors = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ArrayList<Book> getAuthors() {
		return authors;
	}
	public void setAuthors(ArrayList<Book> authors) {
		this.authors = authors;
	}	
}
