package uni.fmi.bachelors.se;

import java.util.ArrayList;

public class Genre {
	long id = -1;
	String name;	
	ArrayList<Book> genres = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ArrayList<Book> getGenres() {
		return genres;
	}
	public void setGenres(ArrayList<Book> genres) {
		this.genres = genres;
	}
}
