package uni.fmi.bachelors.se;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AuthorGenreAddEditForm extends JFrame
{
	private JPanel contentPane;
	AuthorsGenresForm parentForm;
	Author author;
	private JTextField tfName;
	Genre genre;
	int marker;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					AuthorGenreAddEditForm frame = new AuthorGenreAddEditForm(new AuthorsGenresForm(new BooksForm(), 0),
							0);
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @wbp.parser.constructor
	 */
	public AuthorGenreAddEditForm(AuthorsGenresForm form, Author author)
	{
		this(form, 1);
		this.author = author;
		tfName.setText(author.name);
	}

	public AuthorGenreAddEditForm(AuthorsGenresForm form, Genre genre)
	{
		this(form, 2);
		this.genre = genre;
		tfName.setText(genre.name);
	}

	public AuthorGenreAddEditForm(AuthorsGenresForm form, int marker)
	{
		parentForm = form;
		this.marker = marker;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(64, 75, 46, 14);
		contentPane.add(lblTitle);

		tfName = new JTextField();
		tfName.setBounds(143, 72, 206, 20);
		contentPane.add(tfName);
		tfName.setColumns(10);

		JButton btnSaveChanges = new JButton("Save Changes");
		btnSaveChanges.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (tfName.getText().length() > 0)
				{
					if (marker == 1)
					{
						if (author == null)
							author = new Author();
						author.name = tfName.getText();
						if (author.id != -1)
							AuthorRepository.Update(author);
						else
							AuthorRepository.Insert(author);
					} else if (marker == 2)
					{
						if (genre == null)
							genre = new Genre();
						genre.name = tfName.getText();
						if (genre.id != -1)
							GenreRepository.Update(genre);
						else
							GenreRepository.Insert(genre);
					}
					AuthorGenreAddEditForm.this.setVisible(false);
					parentForm.setVisible(true);
					if (marker == 1)
						parentForm.fillAuthorData();
					if (marker == 2)
						parentForm.fillGenreData();
				}
				else {
					JOptionPane optionPane = new JOptionPane("Name Field Empty. Enter Name!", JOptionPane.ERROR_MESSAGE);    
					JDialog dialog = optionPane.createDialog("Error!");
					dialog.setAlwaysOnTop(true);
					dialog.setVisible(true);
				}
			}
		});
		btnSaveChanges.setBounds(64, 169, 112, 23);
		contentPane.add(btnSaveChanges);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				AuthorGenreAddEditForm.this.setVisible(false);
				parentForm.setVisible(true);
				if (marker == 1)
					parentForm.fillAuthorData();
				if (marker == 2)
					parentForm.fillGenreData();
			}
		});
		btnCancel.setBounds(221, 169, 89, 23);
		contentPane.add(btnCancel);
	}
}
